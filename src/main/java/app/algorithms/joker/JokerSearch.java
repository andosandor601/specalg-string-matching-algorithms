package app.algorithms.joker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author And� S�ndor Zsolt
 * 
 *         K�zel�t�ses keres�s jokerrel
 * 
 *         K�nyvben 299/289 oldalon az algoritmus
 * 
 *         Keres�s egy mint�ra, �gy, hogy a joker karakterre elfogadunk b�rmit
 * 
 *         pl: abb�a-t keres�nk �b�aa�b�ab stringen, ekkor 2 tal�lat lesz: abb�a
 *         = �b�aa �s a�b�a -vel
 * 
 */
public class JokerSearch {

	public JokerSearch() {
		super();
	}

	public String search(String inputPattern, String inputString, char joker) {
		return KMP(joker, inputString, inputPattern);
	}

	private String KMP(char joker, String inputString, String inputPattern) {// ab�ab��,
																				// aabbabbaabbabaaaaabbbbbab
		int[] next = new int[inputPattern.length()];
		if (getAbc(inputPattern, joker).equals(getAbc(inputString, joker))) {
			next = computeNext(next, inputPattern, joker);
		}
		int i = 0, j = 0;
		Map<Integer, String> matches = new HashMap<>();
		while (i < inputString.length()) {
			if (checkMatch(inputString.charAt(i), inputPattern.charAt(j), joker)) {
				i++;
				j++;
				if (j == inputPattern.length()) {
					matches.put(i - inputPattern.length(), inputString.substring(i - inputPattern.length(), i));
					j = next[j - 1];
				}
			} else if (j == 0) {
				i++;
			} else {
				j = next[j - 1];
			}
		}
		return makeResult(matches, inputString, inputPattern);
	}

	private List<Character> getAbc(String inputString, char joker) {
		List<Character> abcList = new ArrayList<>();
		for (char letter : inputString.toCharArray()) {
			if (!abcList.contains(letter) && letter != joker) {
				abcList.add(letter);
			}
		}
		return abcList;
	}

	private String makeResult(Map<Integer, String> matches, String inputString, String inputPattern) {
		String result = "";
		for (Map.Entry<Integer, String> entry : matches.entrySet()) {
			result += "\n" + entry.getKey() + "\n";
			result += inputString + "\n";
			for (int i = 0; i < entry.getKey(); i++) {
				result += " ";
			}
			result += inputPattern + "\n";
		}
		return result;
	}

	private boolean checkMatch(char charAt, char charAt2, char joker) {
		return ((charAt == charAt2) || (charAt == joker) || (charAt2 == joker));
	}

	private int[] computeNext(int[] next, String inputPattern, char joker) {
		int j = 1;
		next[0] = 0;
		int i = 0;
		while (j < inputPattern.length()) {
			if (inputPattern.charAt(i) == inputPattern.charAt(j) || inputPattern.charAt(j) == joker) {

				i++;
				j++;
				next[j - 1] = i;
			} else if (i == 0) {
				j++;
				next[j - 1] = 0;
			} else {
				i = next[i - 1];
			}
		}
		return next;
	}

}
