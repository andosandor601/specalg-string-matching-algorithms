package app.algorithms.mismatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * 
 * @author And� S�ndor Zsolt
 * 
 *         K�zel�t�ses mintakeres�s hib�s p�ros�t�ssal
 * 
 *         Robot keres�s (314/304. oldal)
 * 
 *         Figure 8.9: abacbaba-t keres�nk ababcbbababaacbabababbbab-n
 * 
 *         algoritmus outputja 8.10-es �br�n l�that�
 *
 */
public class KMistmatches {

	private List<Queue<Integer>> gArray;

	public KMistmatches() {
		super();
	}

	public String search(String inputPattern, String inputString, int k) {
		preProcess(inputPattern, k);
		return doSearch(inputPattern, inputString, k);
	}

	private void preProcess(String inputPattern, int k) {
		gArray = new ArrayList<>();
		for (int i = 0; i < inputPattern.length(); i++) {
			gArray.add(new LinkedList<>());
		}
		for (int q = 1; q < inputPattern.length() - 1; q++) {
			int i = q;
			while (gArray.get(q).size() < (k * 2 + 1) && i < inputPattern.length()) {
				if (inputPattern.charAt(i) != inputPattern.charAt(i - q)) {
					gArray.get(q).offer(i);
				}
				i++;
			}
		}
	}

	/*
	 * F t�rolja a poz�ci�kat, ahol detekt�lt mismatch volt G[q] n�vekv�
	 * szekvencia a pattern[0...m-q-1] �s pattern[q...m-1] k�z�tti
	 * legbaloldalibb mismatchek
	 * 
	 */
	private String doSearch(String inputPattern, String inputString, int k) {
		Map<Integer, String> result = new HashMap<>();
		Queue<Integer> fQueue = new LinkedList<>();
		Queue<Integer> jQueue = new LinkedList<>();
		Queue<Integer> gQueue = new LinkedList<>();
		int f = -1, g = -1;
		for (int j = 0; j <= inputString.length() - inputPattern.length(); j++) {
			//System.out.println("");
			//System.out.println("j: " + j + " g: " + g);
			if (!fQueue.isEmpty() && fQueue.peek() == (j - f - 1)) {
				//System.out.println("f.poll");
				fQueue.poll();
			}
			if (j <= g) {
				gQueue.clear();
				gQueue.addAll(gArray.get(j - f));
				jQueue = misMerge(f, j, g, new LinkedList<>(fQueue), gQueue, k, inputPattern, inputString);
			} else {
				jQueue.clear();
			}
			//System.out.println("j: " + jQueue);
			if (jQueue.size() <= k) {
				fQueue = jQueue;
				//System.out.println("f: " + fQueue + " j: " + jQueue);
				f = j; // 2
				do {
					g++;
					if (inputPattern.charAt(g - j) != inputString.charAt(g)) {
						fQueue.offer(g - j);// 1,2,3
					}
					//System.out.println("j + inputPattern.length() - 1 " + (j + inputPattern.length() - 1));
				} while (fQueue.size() <= k && g < (j + inputPattern.length() - 1));
				//System.out.println("f: " + fQueue);
				if (fQueue.size() <= k) {
					result.put(j, fQueue.toString());
				}
			}
		}
		return makeResult(result, inputPattern, inputString);
	}

	private String makeResult(Map<Integer, String> matches, String inputPattern, String inputString) {
		String result = matches.toString() + "\n";
		for (Map.Entry<Integer, String> entry : matches.entrySet()) {
			result += "\n" + "hiba helyei: " + entry.getValue() + "\n";
			result += inputString + "\n";
			for (int i = 0; i < entry.getKey(); i++) {
				result += " ";
			}
			result += inputPattern + "\n";
		}
		return result;
	}

	private Queue<Integer> misMerge(int f, int j, int g, Queue<Integer> fQueue, Queue<Integer> gQueue, int k,
			String inputPattern, String inputString) {
		Queue<Integer> jQueue = new LinkedList<>();
		//System.out.println("F input: " + fQueue);
		//System.out.println("G input: " + gQueue);
		//System.out.println("f: " + f + " j: " + j + " g: " + g);
		while (jQueue.size() <= k && !fQueue.isEmpty() && !gQueue.isEmpty()) {

			int p = fQueue.peek(); // 6
			int q = gQueue.peek(); // 3
			if (p < q) {
				fQueue.poll();
				jQueue.offer(p - j + f);
			} else if (q < p) {
				gQueue.poll();// 4,5
				jQueue.offer(q - j + f);// 1
			} else {
				fQueue.poll();
				gQueue.poll();
				if (inputPattern.charAt(p - j + f) != inputString.charAt(f + p)) {
					jQueue.offer(p - j + f);
				}
			}
		}
		while (jQueue.size() <= k && !fQueue.isEmpty()) {
			int p = fQueue.poll();
			jQueue.offer(p - j + f);
		}
		while (jQueue.size() <= k && !gQueue.isEmpty() && gQueue.peek() <= g - f) {
			int q = gQueue.poll(); // 3
			jQueue.offer(q - j + f);
		}
		return jQueue;

	}

}
