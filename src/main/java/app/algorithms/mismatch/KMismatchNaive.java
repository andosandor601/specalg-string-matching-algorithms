package app.algorithms.mismatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.auxiliaries.Pair;

/**
 * 
 * @author And� S�ndor Zsolt
 * 
 *         K�zel�t�ses keres�s k�l�nbs�gekkel
 * 
 *         R�szleges sz�m�t�s (307/297. oldal)
 * 
 *         figure 8.3: GATAA-t keres�nk CAGATAAGAGAA-ben egy k�l�nbs�ggel
 *
 */
public class KMismatchNaive {

	public KMismatchNaive() {
		super();
	}

	public String search(String inputPattern, String inputString, int k) {
		return Naive(k, inputString, inputPattern);
	}

	private String Naive(int k, String inputString, String inputPattern) {
		int endFor = inputString.length() - inputPattern.length();
		List<Pair<Integer, Integer>> matches = new ArrayList<>();
		for (int i = 0; i <= endFor; i++) {
			Pair<Boolean, Integer> pair = findMatch(i, inputString, inputPattern, k);
			if (pair.getFirst()) {
				matches.add(new Pair<>(i + 1, pair.getSecond()));
			}

		}
		return makeResult(matches, inputPattern, inputString);
	}

	private String makeResult(List<Pair<Integer, Integer>> matches, String inputPattern, String inputString) {
		String result = "";
		for (Pair<Integer, Integer> pair : matches) {
			result += "\n" + "hibasz�m: " + pair.getSecond() + "\n";
			result += inputString + "\n";
			for (int i = 0; i < pair.getFirst() - 1; i++) {
				result += " ";
			}
			result += inputPattern + "\n";
		}
		return result;
	}

	private Pair<Boolean, Integer> findMatch(int i, String inputString, String inputPattern, int k) {
		int j = 0;
		int differences = 0;
		while (j < inputPattern.length() && differences <= k) {
			// System.out.println(i + " " + j + " " + inputString.length() + " "
			// + inputPattern.length());
			if (inputString.charAt(i + j) != inputPattern.charAt(j)) {
				// System.out.println(inputString.charAt(i + j) + " == " +
				// inputPattern.charAt(j));
				differences++;
			}
			j++;
		}
		Pair<Boolean, Integer> pair = new Pair<>((j > inputPattern.length() - 1 && differences <= k), differences);
		return pair;
	}

}
