package app.algorithms.shortpattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import app.auxiliaries.VectorOperations;

/**
 * 
 * @author And� S�ndor Zsolt
 * 
 *         K�zel�t� keres�s r�vid mint�ra
 * 
 *         R�vid minta k�l�nbs�gekkel (332/322. oldal)
 *
 */
public class KDiffShortPattern {

	public KDiffShortPattern() {
		super();
	}

	public String search(String inputPattern, String inputString, int k) {
		return kDiff(inputPattern, inputString, k);
	}

	private String kDiff(String inputPattern, String inputString, int k) {
		List<Character> abcList = findABC(inputString);
		Map<Character, Vector<Boolean>> sMap = new HashMap<>();
		List<Vector<Boolean>> rVector = new ArrayList<>();
		Vector<Boolean> tempVector, tempVector2;
		List<Integer> result = new ArrayList<>();

		abcList.forEach(a -> {
			sMap.put(a, VectorOperations.initLogicalVector(inputPattern.length(), true));
		});
		for (int i = 0; i < inputPattern.length(); i++) {
			sMap.get(inputPattern.charAt(i)).set(i, false);
		}
		rVector.add(VectorOperations.initLogicalVector(inputPattern.length(), true));
		for (int l = 1; l <= k; l++) {
			rVector.add(VectorOperations.shiftVector(rVector.get(l - 1)));
		}
		for (int j = 0; j < inputString.length(); j++) {
			tempVector = (Vector<Boolean>) rVector.get(0).clone();
			rVector.set(0, VectorOperations.logicalDisjunction(VectorOperations.shiftVector(rVector.get(0)),
					sMap.get(inputString.charAt(j))));
			for (int l = 1; l <= k; l++) {
				tempVector2 = (Vector<Boolean>) rVector.get(l).clone();
				rVector.set(l, setLRVector(l, rVector, sMap, j, tempVector, inputString));
				tempVector = (Vector<Boolean>) tempVector2.clone();
			}
			// System.out.println(rVector);
			if (rVector.get(k).get(inputPattern.length() - 1) == false) {
				result.add(j);
			}
		}
		return makeResult(result, inputPattern, inputString);
	}

	private String makeResult(List<Integer> resultList, String inputPattern, String inputString) {
		String result = "";
		for (Integer it : resultList) {
			result += "\n" + it + "\n";
			result += inputString + "\n";
			for (int i = 0; i < it - (inputPattern.length()-1); i++) {
				result += " ";
			}
			result += inputPattern + "\n";
		}
		return result;
	}

	private Vector<Boolean> setLRVector(int l, List<Vector<Boolean>> rVector, Map<Character, Vector<Boolean>> sMap,
			int j, Vector<Boolean> tempVector, String inputString) {
		return VectorOperations.logicalConjunction(VectorOperations.logicalConjunction(
				VectorOperations.logicalDisjunction(VectorOperations.shiftVector(rVector.get(l)),
						sMap.get(inputString.charAt(j))),
				VectorOperations.shiftVector(VectorOperations.logicalConjunction(tempVector, rVector.get(l - 1)))),
				tempVector);
	}

	private List<Character> findABC(String inputString) {
		List<Character> abcList = new ArrayList<>();
		for (char letter : inputString.toCharArray()) {
			if (!abcList.contains(letter)) {
				abcList.add(letter);
			}
		}
		return abcList;
	}

}
