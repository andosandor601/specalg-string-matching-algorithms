package app.algorithms.shortpattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import app.auxiliaries.VectorOperations;

/**
 * 
 * @author And� S�ndor Zsolt
 * 
 *         K�zel�t� keres�s r�vid mint�ra
 * 
 *         Pontos egyez�s (324/314. oldal)
 *
 */
public class ShortPatternSearch {

	public ShortPatternSearch() {
		super();
	}

	public String search(String inputPattern, String inputString) {
		return exactMatching(inputPattern, inputString);
	}

	private String exactMatching(String inputPattern, String inputString) {
		List<Character> abcList = findABC(inputString);
		Map<Character, Vector<Boolean>> sMap = new HashMap<>();
		Vector<Boolean> rVector;
		List<Integer> result = new ArrayList<>();

		abcList.forEach(a -> {
			sMap.put(a, VectorOperations.initLogicalVector(inputPattern.length(), true));
		});
		for (int i = 0; i < inputPattern.length(); i++) {
			sMap.get(inputPattern.charAt(i)).set(i, false);
		}
		//System.out.println(sMap);
		rVector = VectorOperations.initLogicalVector(inputPattern.length(), true);
		for (int j = 0; j < inputString.length(); j++) {
			rVector = VectorOperations.logicalDisjunction(VectorOperations.shiftVector(rVector),
					sMap.get(inputString.charAt(j)));
			//System.out.println(j + " " + rVector);
			if (rVector.get(inputPattern.length() - 1) == false) {
				result.add(j - inputPattern.length() + 1);
			}
		}
		return makeResult(result, inputPattern, inputString);

	}

	private String makeResult(List<Integer> resultList, String inputPattern, String inputString) {
		String  result = "";
		for (Integer it : resultList) {
			result += "\n" + it + "\n";
			result += inputString + "\n";
			for (int i = 0; i < it; i++) {
				result += " ";
			}
			result += inputPattern+"\n";
		}
		return result;
	}

	private List<Character> findABC(String inputString) {
		List<Character> abcList = new ArrayList<>();
		for (char letter : inputString.toCharArray()) {
			if (!abcList.contains(letter)) {
				abcList.add(letter);
			}
		}
		return abcList;
	}

}
