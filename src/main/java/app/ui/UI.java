package app.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Scanner;

import app.algorithms.joker.JokerSearch;
import app.algorithms.mismatch.KMismatchNaive;
import app.algorithms.mismatch.KMistmatches;
import app.algorithms.shortpattern.KDiffShortPattern;
import app.algorithms.shortpattern.KMismatchesShortPattern;
import app.algorithms.shortpattern.ShortPatternSearch;

public class UI {
	Scanner sc;
	KMismatchNaive kmn;
	JokerSearch js;
	KMistmatches km;
	KDiffShortPattern kdsp;
	KMismatchesShortPattern kmsp;
	ShortPatternSearch sps;

	public UI() {
		super();
		sc = new Scanner(System.in);
		kmn = new KMismatchNaive();
		js = new JokerSearch();
		km = new KMistmatches();
		kdsp = new KDiffShortPattern();
		kmsp = new KMismatchesShortPattern();
		sps = new ShortPatternSearch();
	}

	public void start() {
		String input = readInput();
		while (input.charAt(0) != 'q') {
			switch (input) {
			case "1":
				runJoker();
				break;
			case "2":
				runKMismatchNaive();
				break;
			case "3":
				runKMismatch();
				break;
			case "4":
				runExactSP();
				break;
			case "5":
				runKDiffSP();
				break;
			case "6":
				runKMismatchSP();
				break;
			case "7":
				runMismatchComparison();
				break;
			default:
				System.out.println("Nem megfelel� input");
				break;
			}
			input = readInput();
		}
		sc.close();
	}

	private void runMismatchComparison() {
		System.out.println("A mismatch algoritmusok �sszehasonl�t�sa, 100-szor a megadott bemenetre:");
		int n = 100;
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		int k;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			k = fsc.nextInt();
			fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.print("K hiba na�v algoritmus: ");
			System.out.println(runKMN(k, pattern, text, n));
			System.out.print("K hiba algoritmus: ");
			System.out.println(runKM(k, pattern, text, n));
			System.out.print("K hiba r�vid mint�ra algoritmus: ");
			System.out.println(runKMSP(k, pattern, text, n));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String runKMSP(int k, String pattern, String text, int n) {
		long startTime = System.currentTimeMillis();
		long total = 0;

		for (int i = 0; i < n; i++) {
			kmsp.search(pattern, text, k);
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		return elapsedTime + "";
	}

	private String runKM(int k, String pattern, String text, int n) {
		long startTime = System.currentTimeMillis();
		long total = 0;

		for (int i = 0; i < n; i++) {
			km.search(pattern, text, k);
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		return elapsedTime + "";
	}

	private String runKMN(int k, String pattern, String text, int n) {
		long startTime = System.currentTimeMillis();
		long total = 0;

		for (int i = 0; i < n; i++) {
			kmn.search(pattern, text, k);
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		return elapsedTime + "";
	}

	private void runKMismatchSP() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		int k;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			k = fsc.nextInt();
			fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Keres�s k hib�val r�vid mint�ra eredm�ny: \n" + kmsp.search(pattern, text, k));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void runKDiffSP() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		int k;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			k = fsc.nextInt();
			fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Keres�s k k�l�nbs�ggel r�vid mint�ra eredm�ny: \n" + kdsp.search(pattern, text, k));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void runExactSP() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Pontos keres�s r�vid mint�ra eredm�ny: \n" + sps.search(pattern, text));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void runKMismatch() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		int k;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			k = fsc.nextInt();
			fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Keres�s k hib�val eredm�nye: \n" + km.search(pattern, text, k));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void runKMismatchNaive() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		int k;
		String pattern, text;
		try {
			fsc = new Scanner(file);
			k = fsc.nextInt();
			fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Keres�s k hib�val (na�v) eredm�nye: \n" + kmn.search(pattern, text, k));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void runJoker() {
		System.out.println("Adja meg a bemeneti f�jlt");
		File file = new File(sc.nextLine());
		Scanner fsc;
		String joker, pattern, text;
		try {
			fsc = new Scanner(file);
			joker = fsc.nextLine();
			pattern = fsc.nextLine();
			text = fsc.nextLine();
			fsc.close();
			System.out.println("Keres�s k hib�val (na�v) eredm�nye: \n" + js.search(pattern, text, joker.charAt(0)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String readInput() {
		System.out.println("\nAdja meg, milyen mintailleszt�st szeretne haszn�lni");
		System.out.println("JokerSerach(joker csak a mint�ban) - '1'");
		System.out.println("Keres�s k hib�val (na�v) - '2'");
		System.out.println("Keres�s k hib�val - '3'");
		System.out.println("Pontos keres�s r�vid mint�ra - '4'");
		System.out.println("Keres�s k k�l�nbs�ggel r�vid mint�ra - '5'");
		System.out.println("Keres�s k hib�val r�vid mint�ra - '6'");
		System.out.println("'2','3','6', �sszehasonl�t�sa - '7'");
		System.out.println("Kil�p�s - 'q'");
		return sc.nextLine();
	}

}
