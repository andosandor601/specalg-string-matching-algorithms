package app;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import app.algorithms.joker.JokerSearch;
import app.algorithms.mismatch.KMismatchNaive;
import app.algorithms.mismatch.KMistmatches;
import app.algorithms.shortpattern.KDiffShortPattern;
import app.algorithms.shortpattern.KMismatchesShortPattern;
import app.algorithms.shortpattern.ShortPatternSearch;
import app.ui.UI;

public class StringMatchingApplication {

	public static void main(String[] args) {
		UI ui = new UI();
		ui.start();
	}

}
