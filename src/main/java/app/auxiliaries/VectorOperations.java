package app.auxiliaries;

import java.util.Vector;

public final class VectorOperations {

	private VectorOperations() {
		super();
	}

	public static Vector<Boolean> initLogicalVector(int length, boolean value) {
		Vector<Boolean> vector = new Vector<>(length);
		for (int i = 0; i < length; i++) {
			vector.add(value);
		}
		return vector;
	}

	public static Vector<Boolean> shiftVector(Vector<Boolean> rVector) {
		Vector<Boolean> vector = (Vector<Boolean>) rVector.clone();
		for (int i = vector.size() - 1; i > 0; i--) {
			vector.set(i, vector.get(i - 1));
		}
		vector.set(0, false);
		return vector;
	}

	public static Vector<Boolean> logicalDisjunction(Vector<Boolean> vector1, Vector<Boolean> vector2) {
		Vector<Boolean> resultVector = (Vector<Boolean>) vector1.clone();
		for (int i = 0; i < resultVector.size(); i++) {
			resultVector.set(i, vector1.get(i) | vector2.get(i));
		}
		return resultVector;
	}

	public static Vector<Boolean> logicalConjunction(Vector<Boolean> vector1, Vector<Boolean> vector2) {
		Vector<Boolean> resultVector = (Vector<Boolean>) vector1.clone();
		for (int i = 0; i < resultVector.size(); i++) {
			resultVector.set(i, vector1.get(i) & vector2.get(i));
		}
		return resultVector;
	}

}
